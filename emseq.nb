(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 8.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     28294,        948]
NotebookOptionsPosition[     20495,        732]
NotebookOutlinePosition[     20832,        747]
CellTagsIndexPosition[     20789,        744]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"em", "=", "\"\<010\>\""}], ";"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{"For", "[", 
  RowBox[{
   RowBox[{"j", "=", "1"}], ",", 
   RowBox[{"j", "<", "100"}], ",", 
   RowBox[{"j", "++"}], ",", "\[IndentingNewLine]", 
   RowBox[{"(*", 
    RowBox[{"j", "<", 
     RowBox[{
     "10000", " ", "would", " ", "generate", " ", "the", " ", "first", " ", 
      "10000", " ", "bits"}]}], "*)"}], "\[IndentingNewLine]", 
   RowBox[{"For", "[", 
    RowBox[{
     RowBox[{"i", "=", "1"}], ",", 
     RowBox[{"i", "<", 
      RowBox[{"StringLength", "[", "em", "]"}]}], ",", 
     RowBox[{"i", "++"}], ",", "\[IndentingNewLine]", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"suff", "=", 
       RowBox[{"StringTake", "[", 
        RowBox[{"em", ",", 
         RowBox[{"-", "i"}]}], "]"}]}], ";", "\[IndentingNewLine]", 
      RowBox[{"matchings", "=", 
       RowBox[{"StringPosition", "[", 
        RowBox[{"em", ",", "suff"}], "]"}]}], ";", "\[IndentingNewLine]", 
      RowBox[{"If", "[", 
       RowBox[{
        RowBox[{
         RowBox[{"Length", "[", "matchings", "]"}], "==", " ", "1"}], ",", 
        "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{"Print", "[", 
          RowBox[{"i", "-", "1"}], "]"}], ";", 
         RowBox[{"(*", 
          RowBox[{"match", " ", "length"}], "*)"}], "\[IndentingNewLine]", 
         RowBox[{"Print", "[", 
          RowBox[{"StringTake", "[", 
           RowBox[{"em", ",", 
            RowBox[{"-", 
             RowBox[{"(", 
              RowBox[{"i", "-", "1"}], ")"}]}]}], "]"}], "]"}], ";", 
         RowBox[{"(*", 
          RowBox[{"matching", " ", "string"}], "*)"}], "\[IndentingNewLine]", 
         
         RowBox[{"lastoccur", "=", 
          RowBox[{"StringPosition", "[", 
           RowBox[{"em", ",", 
            RowBox[{"StringTake", "[", 
             RowBox[{"em", ",", 
              RowBox[{"-", 
               RowBox[{"(", 
                RowBox[{"i", "-", "1"}], ")"}]}]}], "]"}]}], "]"}]}], ";", 
         "\[IndentingNewLine]", 
         RowBox[{"(*", 
          RowBox[{
           RowBox[{"Print", "[", 
            RowBox[{
             RowBox[{
              RowBox[{"(", 
               RowBox[{"lastoccur", "[", 
                RowBox[{"[", 
                 RowBox[{"-", "2"}], "]"}], "]"}], ")"}], "[", 
              RowBox[{"[", "2", "]"}], "]"}], "+", "1"}], "]"}], ";", "     ", 
           RowBox[{
           "could", " ", "be", " ", "used", " ", "to", " ", "print", " ", 
            "the", " ", "position", " ", "of", " ", "match"}]}], "*)"}], " ", 
         "\[IndentingNewLine]", 
         RowBox[{"chosen", "=", 
          RowBox[{
           RowBox[{
            RowBox[{"(", 
             RowBox[{"lastoccur", "[", 
              RowBox[{"[", 
               RowBox[{"-", "2"}], "]"}], "]"}], ")"}], "[", 
            RowBox[{"[", "2", "]"}], "]"}], "+", "1"}]}], ";", "  ", 
         RowBox[{"(*", 
          RowBox[{
           RowBox[{
            RowBox[{"-", "2"}], " ", "means", " ", "the", " ", "second", " ", 
            "last", " ", "occurence", " ", "of", " ", "the", " ", "substring",
             " ", "in", " ", "em"}], " ", ",", " ", 
           RowBox[{
           "use", " ", "1", " ", "for", " ", "1", "st", " ", "ocurence"}]}], 
          "*)"}], "\[IndentingNewLine]", 
         RowBox[{"bit", "=", 
          RowBox[{"StringTake", "[", 
           RowBox[{"em", ",", 
            RowBox[{"{", "chosen", "}"}]}], "]"}]}], ";", 
         "\[IndentingNewLine]", 
         RowBox[{"If", "[", 
          RowBox[{
           RowBox[{"bit", "\[Equal]", "\"\<0\>\""}], ",", 
           RowBox[{"bit", "=", "\"\<1\>\""}], ",", 
           RowBox[{"bit", "=", "\"\<0\>\""}]}], "]"}], ";", 
         "\[IndentingNewLine]", 
         RowBox[{"em", "=", 
          RowBox[{"StringJoin", "[", 
           RowBox[{"em", ",", "bit"}], "]"}]}], ";", "\[IndentingNewLine]", 
         RowBox[{"Break", "[", "]"}], ";"}]}], "\[IndentingNewLine]", 
       "]"}]}]}], "\[IndentingNewLine]", "\[IndentingNewLine]", "]"}]}], 
  "\[IndentingNewLine]", "\[IndentingNewLine]", 
  "]"}], "\[IndentingNewLine]", "em"}], "Input"],

Cell[CellGroupData[{

Cell[BoxData["1"], "Print",
 CellChangeTimes->{3.610501032392911*^9}],

Cell[BoxData["\<\"0\"\>"], "Print",
 CellChangeTimes->{3.610501032395628*^9}],

Cell[BoxData["1"], "Print",
 CellChangeTimes->{3.610501032398089*^9}],

Cell[BoxData["\<\"0\"\>"], "Print",
 CellChangeTimes->{3.610501032400346*^9}],

Cell[BoxData["2"], "Print",
 CellChangeTimes->{3.610501032402673*^9}],

Cell[BoxData["\<\"01\"\>"], "Print",
 CellChangeTimes->{3.610501032405047*^9}],

Cell[BoxData["1"], "Print",
 CellChangeTimes->{3.6105010324074783`*^9}],

Cell[BoxData["\<\"1\"\>"], "Print",
 CellChangeTimes->{3.61050103240971*^9}],

Cell[BoxData["2"], "Print",
 CellChangeTimes->{3.6105010324120007`*^9}],

Cell[BoxData["\<\"10\"\>"], "Print",
 CellChangeTimes->{3.610501032414296*^9}],

Cell[BoxData["2"], "Print",
 CellChangeTimes->{3.6105010324165163`*^9}],

Cell[BoxData["\<\"01\"\>"], "Print",
 CellChangeTimes->{3.610501032418723*^9}],

Cell[BoxData["3"], "Print",
 CellChangeTimes->{3.610501032420923*^9}],

Cell[BoxData["\<\"010\"\>"], "Print",
 CellChangeTimes->{3.610501032423306*^9}],

Cell[BoxData["3"], "Print",
 CellChangeTimes->{3.6105010324257517`*^9}],

Cell[BoxData["\<\"101\"\>"], "Print",
 CellChangeTimes->{3.610501032428388*^9}],

Cell[BoxData["3"], "Print",
 CellChangeTimes->{3.610501032431114*^9}],

Cell[BoxData["\<\"011\"\>"], "Print",
 CellChangeTimes->{3.6105010324339237`*^9}],

Cell[BoxData["2"], "Print",
 CellChangeTimes->{3.610501032436575*^9}],

Cell[BoxData["\<\"11\"\>"], "Print",
 CellChangeTimes->{3.610501032439664*^9}],

Cell[BoxData["3"], "Print",
 CellChangeTimes->{3.610501032443327*^9}],

Cell[BoxData["\<\"110\"\>"], "Print",
 CellChangeTimes->{3.6105010324472933`*^9}],

Cell[BoxData["3"], "Print",
 CellChangeTimes->{3.610501032451572*^9}],

Cell[BoxData["\<\"100\"\>"], "Print",
 CellChangeTimes->{3.6105010324544287`*^9}],

Cell[BoxData["2"], "Print",
 CellChangeTimes->{3.610501032456675*^9}],

Cell[BoxData["\<\"00\"\>"], "Print",
 CellChangeTimes->{3.610501032459063*^9}],

Cell[BoxData["3"], "Print",
 CellChangeTimes->{3.610501032461331*^9}],

Cell[BoxData["\<\"001\"\>"], "Print",
 CellChangeTimes->{3.610501032463595*^9}],

Cell[BoxData["3"], "Print",
 CellChangeTimes->{3.6105010324659443`*^9}],

Cell[BoxData["\<\"010\"\>"], "Print",
 CellChangeTimes->{3.610501032468711*^9}],

Cell[BoxData["4"], "Print",
 CellChangeTimes->{3.610501032471743*^9}],

Cell[BoxData["\<\"0100\"\>"], "Print",
 CellChangeTimes->{3.610501032474519*^9}],

Cell[BoxData["4"], "Print",
 CellChangeTimes->{3.61050103247701*^9}],

Cell[BoxData["\<\"1000\"\>"], "Print",
 CellChangeTimes->{3.610501032480052*^9}],

Cell[BoxData["3"], "Print",
 CellChangeTimes->{3.6105010324830236`*^9}],

Cell[BoxData["\<\"000\"\>"], "Print",
 CellChangeTimes->{3.610501032485463*^9}],

Cell[BoxData["4"], "Print",
 CellChangeTimes->{3.610501032487913*^9}],

Cell[BoxData["\<\"0001\"\>"], "Print",
 CellChangeTimes->{3.61050103249032*^9}],

Cell[BoxData["4"], "Print",
 CellChangeTimes->{3.6105010324927607`*^9}],

Cell[BoxData["\<\"0011\"\>"], "Print",
 CellChangeTimes->{3.610501032495202*^9}],

Cell[BoxData["4"], "Print",
 CellChangeTimes->{3.610501032497608*^9}],

Cell[BoxData["\<\"0111\"\>"], "Print",
 CellChangeTimes->{3.610501032499918*^9}],

Cell[BoxData["3"], "Print",
 CellChangeTimes->{3.610501032502264*^9}],

Cell[BoxData["\<\"111\"\>"], "Print",
 CellChangeTimes->{3.610501032504856*^9}],

Cell[BoxData["4"], "Print",
 CellChangeTimes->{3.610501032507574*^9}],

Cell[BoxData["\<\"1110\"\>"], "Print",
 CellChangeTimes->{3.610501032510754*^9}],

Cell[BoxData["4"], "Print",
 CellChangeTimes->{3.6105010325136003`*^9}],

Cell[BoxData["\<\"1101\"\>"], "Print",
 CellChangeTimes->{3.610501032516387*^9}],

Cell[BoxData["4"], "Print",
 CellChangeTimes->{3.610501032519747*^9}],

Cell[BoxData["\<\"1011\"\>"], "Print",
 CellChangeTimes->{3.610501032523012*^9}],

Cell[BoxData["4"], "Print",
 CellChangeTimes->{3.610501032526258*^9}],

Cell[BoxData["\<\"0110\"\>"], "Print",
 CellChangeTimes->{3.610501032529811*^9}],

Cell[BoxData["4"], "Print",
 CellChangeTimes->{3.6105010325342627`*^9}],

Cell[BoxData["\<\"1100\"\>"], "Print",
 CellChangeTimes->{3.6105010325390587`*^9}],

Cell[BoxData["4"], "Print",
 CellChangeTimes->{3.610501032543333*^9}],

Cell[BoxData["\<\"1001\"\>"], "Print",
 CellChangeTimes->{3.61050103254673*^9}],

Cell[BoxData["4"], "Print",
 CellChangeTimes->{3.610501032550356*^9}],

Cell[BoxData["\<\"0010\"\>"], "Print",
 CellChangeTimes->{3.610501032554068*^9}],

Cell[BoxData["4"], "Print",
 CellChangeTimes->{3.610501032557526*^9}],

Cell[BoxData["\<\"0101\"\>"], "Print",
 CellChangeTimes->{3.6105010325610657`*^9}],

Cell[BoxData["4"], "Print",
 CellChangeTimes->{3.610501032564636*^9}],

Cell[BoxData["\<\"1010\"\>"], "Print",
 CellChangeTimes->{3.610501032568527*^9}],

Cell[BoxData["4"], "Print",
 CellChangeTimes->{3.61050103257269*^9}],

Cell[BoxData["\<\"0100\"\>"], "Print",
 CellChangeTimes->{3.610501032576439*^9}],

Cell[BoxData["5"], "Print",
 CellChangeTimes->{3.610501032579605*^9}],

Cell[BoxData["\<\"01001\"\>"], "Print",
 CellChangeTimes->{3.6105010325828867`*^9}],

Cell[BoxData["5"], "Print",
 CellChangeTimes->{3.6105010325859337`*^9}],

Cell[BoxData["\<\"10010\"\>"], "Print",
 CellChangeTimes->{3.610501032588891*^9}],

Cell[BoxData["5"], "Print",
 CellChangeTimes->{3.610501032591753*^9}],

Cell[BoxData["\<\"00100\"\>"], "Print",
 CellChangeTimes->{3.610501032594695*^9}],

Cell[BoxData["5"], "Print",
 CellChangeTimes->{3.610501032597807*^9}],

Cell[BoxData["\<\"01001\"\>"], "Print",
 CellChangeTimes->{3.610501032601478*^9}],

Cell[BoxData["6"], "Print",
 CellChangeTimes->{3.6105010326053467`*^9}],

Cell[BoxData["\<\"010011\"\>"], "Print",
 CellChangeTimes->{3.6105010326095247`*^9}],

Cell[BoxData["5"], "Print",
 CellChangeTimes->{3.6105010326134167`*^9}],

Cell[BoxData["\<\"00111\"\>"], "Print",
 CellChangeTimes->{3.6105010326163607`*^9}],

Cell[BoxData["5"], "Print",
 CellChangeTimes->{3.610501032619314*^9}],

Cell[BoxData["\<\"01110\"\>"], "Print",
 CellChangeTimes->{3.6105010326222*^9}],

Cell[BoxData["5"], "Print",
 CellChangeTimes->{3.610501032625601*^9}],

Cell[BoxData["\<\"11101\"\>"], "Print",
 CellChangeTimes->{3.610501032629776*^9}],

Cell[BoxData["5"], "Print",
 CellChangeTimes->{3.610501032633988*^9}],

Cell[BoxData["\<\"11010\"\>"], "Print",
 CellChangeTimes->{3.610501032637793*^9}],

Cell[BoxData["5"], "Print",
 CellChangeTimes->{3.61050103264078*^9}],

Cell[BoxData["\<\"10100\"\>"], "Print",
 CellChangeTimes->{3.6105010326436462`*^9}],

Cell[BoxData["5"], "Print",
 CellChangeTimes->{3.610501032646587*^9}],

Cell[BoxData["\<\"01000\"\>"], "Print",
 CellChangeTimes->{3.610501032649341*^9}],

Cell[BoxData["5"], "Print",
 CellChangeTimes->{3.610501032651988*^9}],

Cell[BoxData["\<\"10001\"\>"], "Print",
 CellChangeTimes->{3.610501032654793*^9}],

Cell[BoxData["5"], "Print",
 CellChangeTimes->{3.610501032657568*^9}],

Cell[BoxData["\<\"00011\"\>"], "Print",
 CellChangeTimes->{3.610501032660304*^9}],

Cell[BoxData["5"], "Print",
 CellChangeTimes->{3.610501032663278*^9}],

Cell[BoxData["\<\"00110\"\>"], "Print",
 CellChangeTimes->{3.6105010326661797`*^9}],

Cell[BoxData["5"], "Print",
 CellChangeTimes->{3.610501032669038*^9}],

Cell[BoxData["\<\"01100\"\>"], "Print",
 CellChangeTimes->{3.610501032671754*^9}],

Cell[BoxData["5"], "Print",
 CellChangeTimes->{3.610501032674428*^9}],

Cell[BoxData["\<\"11000\"\>"], "Print",
 CellChangeTimes->{3.610501032677279*^9}],

Cell[BoxData["5"], "Print",
 CellChangeTimes->{3.610501032680018*^9}],

Cell[BoxData["\<\"10000\"\>"], "Print",
 CellChangeTimes->{3.610501032682802*^9}],

Cell[BoxData["4"], "Print",
 CellChangeTimes->{3.610501032685596*^9}],

Cell[BoxData["\<\"0000\"\>"], "Print",
 CellChangeTimes->{3.610501032688456*^9}],

Cell[BoxData["5"], "Print",
 CellChangeTimes->{3.610501032691449*^9}],

Cell[BoxData["\<\"00001\"\>"], "Print",
 CellChangeTimes->{3.610501032694363*^9}],

Cell[BoxData["5"], "Print",
 CellChangeTimes->{3.610501032697288*^9}],

Cell[BoxData["\<\"00010\"\>"], "Print",
 CellChangeTimes->{3.610501032700295*^9}],

Cell[BoxData["5"], "Print",
 CellChangeTimes->{3.6105010327069817`*^9}],

Cell[BoxData["\<\"00101\"\>"], "Print",
 CellChangeTimes->{3.6105010327100286`*^9}],

Cell[BoxData["5"], "Print",
 CellChangeTimes->{3.610501032713272*^9}],

Cell[BoxData["\<\"01011\"\>"], "Print",
 CellChangeTimes->{3.610501032716228*^9}],

Cell[BoxData["5"], "Print",
 CellChangeTimes->{3.610501032719142*^9}],

Cell[BoxData["\<\"10110\"\>"], "Print",
 CellChangeTimes->{3.6105010327220097`*^9}],

Cell[BoxData["5"], "Print",
 CellChangeTimes->{3.6105010327248907`*^9}],

Cell[BoxData["\<\"01101\"\>"], "Print",
 CellChangeTimes->{3.610501032727977*^9}],

Cell[BoxData["5"], "Print",
 CellChangeTimes->{3.6105010327310143`*^9}],

Cell[BoxData["\<\"11011\"\>"], "Print",
 CellChangeTimes->{3.610501032733972*^9}],

Cell[BoxData["5"], "Print",
 CellChangeTimes->{3.610501032736812*^9}],

Cell[BoxData["\<\"10111\"\>"], "Print",
 CellChangeTimes->{3.610501032739811*^9}],

Cell[BoxData["5"], "Print",
 CellChangeTimes->{3.610501032742558*^9}],

Cell[BoxData["\<\"01111\"\>"], "Print",
 CellChangeTimes->{3.610501032745241*^9}],

Cell[BoxData["4"], "Print",
 CellChangeTimes->{3.610501032748199*^9}],

Cell[BoxData["\<\"1111\"\>"], "Print",
 CellChangeTimes->{3.610501032751153*^9}],

Cell[BoxData["5"], "Print",
 CellChangeTimes->{3.610501032754163*^9}],

Cell[BoxData["\<\"11110\"\>"], "Print",
 CellChangeTimes->{3.6105010327573*^9}],

Cell[BoxData["5"], "Print",
 CellChangeTimes->{3.610501032760208*^9}],

Cell[BoxData["\<\"11100\"\>"], "Print",
 CellChangeTimes->{3.610501032763052*^9}],

Cell[BoxData["5"], "Print",
 CellChangeTimes->{3.610501032765917*^9}],

Cell[BoxData["\<\"11001\"\>"], "Print",
 CellChangeTimes->{3.6105010327685747`*^9}],

Cell[BoxData["5"], "Print",
 CellChangeTimes->{3.610501032771402*^9}],

Cell[BoxData["\<\"10011\"\>"], "Print",
 CellChangeTimes->{3.610501032774302*^9}],

Cell[BoxData["6"], "Print",
 CellChangeTimes->{3.610501032777361*^9}],

Cell[BoxData["\<\"100110\"\>"], "Print",
 CellChangeTimes->{3.610501032780155*^9}],

Cell[BoxData["6"], "Print",
 CellChangeTimes->{3.610501032783066*^9}],

Cell[BoxData["\<\"001100\"\>"], "Print",
 CellChangeTimes->{3.610501032785977*^9}],

Cell[BoxData["6"], "Print",
 CellChangeTimes->{3.6105010327891817`*^9}],

Cell[BoxData["\<\"011001\"\>"], "Print",
 CellChangeTimes->{3.6105010327923183`*^9}],

Cell[BoxData["6"], "Print",
 CellChangeTimes->{3.6105010327953176`*^9}],

Cell[BoxData["\<\"110011\"\>"], "Print",
 CellChangeTimes->{3.6105010327983027`*^9}],

Cell[BoxData["6"], "Print",
 CellChangeTimes->{3.610501032801062*^9}],

Cell[BoxData["\<\"100111\"\>"], "Print",
 CellChangeTimes->{3.610501032803864*^9}],

Cell[BoxData["6"], "Print",
 CellChangeTimes->{3.6105010328066483`*^9}],

Cell[BoxData["\<\"001111\"\>"], "Print",
 CellChangeTimes->{3.61050103280932*^9}],

Cell[BoxData["6"], "Print",
 CellChangeTimes->{3.61050103281205*^9}],

Cell[BoxData["\<\"011111\"\>"], "Print",
 CellChangeTimes->{3.610501032814755*^9}],

Cell[BoxData["5"], "Print",
 CellChangeTimes->{3.610501032817477*^9}],

Cell[BoxData["\<\"11111\"\>"], "Print",
 CellChangeTimes->{3.61050103282018*^9}],

Cell[BoxData["6"], "Print",
 CellChangeTimes->{3.6105010328229713`*^9}],

Cell[BoxData["\<\"111110\"\>"], "Print",
 CellChangeTimes->{3.610501032825794*^9}],

Cell[BoxData["6"], "Print",
 CellChangeTimes->{3.610501032828578*^9}],

Cell[BoxData["\<\"111101\"\>"], "Print",
 CellChangeTimes->{3.610501032831286*^9}],

Cell[BoxData["6"], "Print",
 CellChangeTimes->{3.610501032834023*^9}],

Cell[BoxData["\<\"111010\"\>"], "Print",
 CellChangeTimes->{3.6105010328367777`*^9}],

Cell[BoxData["6"], "Print",
 CellChangeTimes->{3.61050103283965*^9}],

Cell[BoxData["\<\"110101\"\>"], "Print",
 CellChangeTimes->{3.610501032842346*^9}],

Cell[BoxData["5"], "Print",
 CellChangeTimes->{3.610501032845065*^9}],

Cell[BoxData["\<\"01010\"\>"], "Print",
 CellChangeTimes->{3.610501032848207*^9}],

Cell[BoxData["5"], "Print",
 CellChangeTimes->{3.610501032850913*^9}],

Cell[BoxData["\<\"10101\"\>"], "Print",
 CellChangeTimes->{3.610501032853717*^9}],

Cell[BoxData["6"], "Print",
 CellChangeTimes->{3.610501032856831*^9}],

Cell[BoxData["\<\"101011\"\>"], "Print",
 CellChangeTimes->{3.610501032860094*^9}],

Cell[BoxData["6"], "Print",
 CellChangeTimes->{3.610501032863511*^9}],

Cell[BoxData["\<\"010110\"\>"], "Print",
 CellChangeTimes->{3.61050103286655*^9}],

Cell[BoxData["6"], "Print",
 CellChangeTimes->{3.6105010328694277`*^9}],

Cell[BoxData["\<\"101100\"\>"], "Print",
 CellChangeTimes->{3.610501032872408*^9}],

Cell[BoxData["6"], "Print",
 CellChangeTimes->{3.610501032875421*^9}],

Cell[BoxData["\<\"011000\"\>"], "Print",
 CellChangeTimes->{3.610501032879096*^9}],

Cell[BoxData["6"], "Print",
 CellChangeTimes->{3.610501032885001*^9}],

Cell[BoxData["\<\"110001\"\>"], "Print",
 CellChangeTimes->{3.610501032891034*^9}],

Cell[BoxData["6"], "Print",
 CellChangeTimes->{3.610501032895013*^9}],

Cell[BoxData["\<\"100011\"\>"], "Print",
 CellChangeTimes->{3.610501032899376*^9}],

Cell[BoxData["6"], "Print",
 CellChangeTimes->{3.610501032903006*^9}],

Cell[BoxData["\<\"000111\"\>"], "Print",
 CellChangeTimes->{3.610501032906637*^9}],

Cell[BoxData["6"], "Print",
 CellChangeTimes->{3.6105010329104013`*^9}],

Cell[BoxData["\<\"001110\"\>"], "Print",
 CellChangeTimes->{3.610501032914016*^9}],

Cell[BoxData["6"], "Print",
 CellChangeTimes->{3.610501032917754*^9}],

Cell[BoxData["\<\"011100\"\>"], "Print",
 CellChangeTimes->{3.610501032921322*^9}],

Cell[BoxData["6"], "Print",
 CellChangeTimes->{3.6105010329247026`*^9}],

Cell[BoxData["\<\"111001\"\>"], "Print",
 CellChangeTimes->{3.610501032927991*^9}],

Cell[BoxData["6"], "Print",
 CellChangeTimes->{3.610501032931179*^9}],

Cell[BoxData["\<\"110010\"\>"], "Print",
 CellChangeTimes->{3.61050103293443*^9}],

Cell[BoxData["6"], "Print",
 CellChangeTimes->{3.610501032937718*^9}],

Cell[BoxData["\<\"100100\"\>"], "Print",
 CellChangeTimes->{3.610501032940453*^9}],

Cell[BoxData["6"], "Print",
 CellChangeTimes->{3.61050103294343*^9}],

Cell[BoxData["\<\"001000\"\>"], "Print",
 CellChangeTimes->{3.610501032946433*^9}],

Cell[BoxData["6"], "Print",
 CellChangeTimes->{3.610501032949918*^9}],

Cell[BoxData["\<\"010001\"\>"], "Print",
 CellChangeTimes->{3.6105010329548807`*^9}],

Cell[BoxData["6"], "Print",
 CellChangeTimes->{3.610501032959485*^9}],

Cell[BoxData["\<\"100010\"\>"], "Print",
 CellChangeTimes->{3.610501032963133*^9}],

Cell[BoxData["6"], "Print",
 CellChangeTimes->{3.6105010329663153`*^9}],

Cell[BoxData["\<\"000101\"\>"], "Print",
 CellChangeTimes->{3.610501032969595*^9}],

Cell[BoxData["6"], "Print",
 CellChangeTimes->{3.610501032974422*^9}],

Cell[BoxData["\<\"001010\"\>"], "Print",
 CellChangeTimes->{3.6105010329792*^9}],

Cell[BoxData["6"], "Print",
 CellChangeTimes->{3.610501032982381*^9}],

Cell[BoxData["\<\"010101\"\>"], "Print",
 CellChangeTimes->{3.6105010329854794`*^9}],

Cell[BoxData["6"], "Print",
 CellChangeTimes->{3.610501032988701*^9}],

Cell[BoxData["\<\"101010\"\>"], "Print",
 CellChangeTimes->{3.610501032991788*^9}],

Cell[BoxData["6"], "Print",
 CellChangeTimes->{3.6105010329949093`*^9}],

Cell[BoxData["\<\"010100\"\>"], "Print",
 CellChangeTimes->{3.610501032998011*^9}],

Cell[BoxData["6"], "Print",
 CellChangeTimes->{3.610501033000813*^9}],

Cell[BoxData["\<\"101000\"\>"], "Print",
 CellChangeTimes->{3.610501033003681*^9}]
}, Open  ]],

Cell[BoxData["\<\"\
010011010111000100001111011001010010011101000110000010110111110011001111110101\
011000111001000101010000\"\>"], "Output",
 CellChangeTimes->{3.610501033006628*^9}]
}, Open  ]]
},
WindowSize->{740, 598},
WindowMargins->{{162, Automatic}, {Automatic, 35}},
FrontEndVersion->"8.0 for Linux x86 (64-bit) (October 10, 2011)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[579, 22, 4200, 105, 525, "Input"],
Cell[CellGroupData[{
Cell[4804, 131, 69, 1, 23, "Print"],
Cell[4876, 134, 77, 1, 23, "Print"],
Cell[4956, 137, 69, 1, 23, "Print"],
Cell[5028, 140, 77, 1, 23, "Print"],
Cell[5108, 143, 69, 1, 23, "Print"],
Cell[5180, 146, 78, 1, 23, "Print"],
Cell[5261, 149, 71, 1, 23, "Print"],
Cell[5335, 152, 76, 1, 23, "Print"],
Cell[5414, 155, 71, 1, 23, "Print"],
Cell[5488, 158, 78, 1, 23, "Print"],
Cell[5569, 161, 71, 1, 23, "Print"],
Cell[5643, 164, 78, 1, 23, "Print"],
Cell[5724, 167, 69, 1, 23, "Print"],
Cell[5796, 170, 79, 1, 23, "Print"],
Cell[5878, 173, 71, 1, 23, "Print"],
Cell[5952, 176, 79, 1, 23, "Print"],
Cell[6034, 179, 69, 1, 23, "Print"],
Cell[6106, 182, 81, 1, 23, "Print"],
Cell[6190, 185, 69, 1, 23, "Print"],
Cell[6262, 188, 78, 1, 23, "Print"],
Cell[6343, 191, 69, 1, 23, "Print"],
Cell[6415, 194, 81, 1, 23, "Print"],
Cell[6499, 197, 69, 1, 23, "Print"],
Cell[6571, 200, 81, 1, 23, "Print"],
Cell[6655, 203, 69, 1, 23, "Print"],
Cell[6727, 206, 78, 1, 23, "Print"],
Cell[6808, 209, 69, 1, 23, "Print"],
Cell[6880, 212, 79, 1, 23, "Print"],
Cell[6962, 215, 71, 1, 23, "Print"],
Cell[7036, 218, 79, 1, 23, "Print"],
Cell[7118, 221, 69, 1, 23, "Print"],
Cell[7190, 224, 80, 1, 23, "Print"],
Cell[7273, 227, 68, 1, 23, "Print"],
Cell[7344, 230, 80, 1, 23, "Print"],
Cell[7427, 233, 71, 1, 23, "Print"],
Cell[7501, 236, 79, 1, 23, "Print"],
Cell[7583, 239, 69, 1, 23, "Print"],
Cell[7655, 242, 79, 1, 23, "Print"],
Cell[7737, 245, 71, 1, 23, "Print"],
Cell[7811, 248, 80, 1, 23, "Print"],
Cell[7894, 251, 69, 1, 23, "Print"],
Cell[7966, 254, 80, 1, 23, "Print"],
Cell[8049, 257, 69, 1, 23, "Print"],
Cell[8121, 260, 79, 1, 23, "Print"],
Cell[8203, 263, 69, 1, 23, "Print"],
Cell[8275, 266, 80, 1, 23, "Print"],
Cell[8358, 269, 71, 1, 23, "Print"],
Cell[8432, 272, 80, 1, 23, "Print"],
Cell[8515, 275, 69, 1, 23, "Print"],
Cell[8587, 278, 80, 1, 23, "Print"],
Cell[8670, 281, 69, 1, 23, "Print"],
Cell[8742, 284, 80, 1, 23, "Print"],
Cell[8825, 287, 71, 1, 23, "Print"],
Cell[8899, 290, 82, 1, 23, "Print"],
Cell[8984, 293, 69, 1, 23, "Print"],
Cell[9056, 296, 79, 1, 23, "Print"],
Cell[9138, 299, 69, 1, 23, "Print"],
Cell[9210, 302, 80, 1, 23, "Print"],
Cell[9293, 305, 69, 1, 23, "Print"],
Cell[9365, 308, 82, 1, 23, "Print"],
Cell[9450, 311, 69, 1, 23, "Print"],
Cell[9522, 314, 80, 1, 23, "Print"],
Cell[9605, 317, 68, 1, 23, "Print"],
Cell[9676, 320, 80, 1, 23, "Print"],
Cell[9759, 323, 69, 1, 23, "Print"],
Cell[9831, 326, 83, 1, 23, "Print"],
Cell[9917, 329, 71, 1, 23, "Print"],
Cell[9991, 332, 81, 1, 23, "Print"],
Cell[10075, 335, 69, 1, 23, "Print"],
Cell[10147, 338, 81, 1, 23, "Print"],
Cell[10231, 341, 69, 1, 23, "Print"],
Cell[10303, 344, 81, 1, 23, "Print"],
Cell[10387, 347, 71, 1, 23, "Print"],
Cell[10461, 350, 84, 1, 23, "Print"],
Cell[10548, 353, 71, 1, 23, "Print"],
Cell[10622, 356, 83, 1, 23, "Print"],
Cell[10708, 359, 69, 1, 23, "Print"],
Cell[10780, 362, 79, 1, 23, "Print"],
Cell[10862, 365, 69, 1, 23, "Print"],
Cell[10934, 368, 81, 1, 23, "Print"],
Cell[11018, 371, 69, 1, 23, "Print"],
Cell[11090, 374, 81, 1, 23, "Print"],
Cell[11174, 377, 68, 1, 23, "Print"],
Cell[11245, 380, 83, 1, 23, "Print"],
Cell[11331, 383, 69, 1, 23, "Print"],
Cell[11403, 386, 81, 1, 23, "Print"],
Cell[11487, 389, 69, 1, 23, "Print"],
Cell[11559, 392, 81, 1, 23, "Print"],
Cell[11643, 395, 69, 1, 23, "Print"],
Cell[11715, 398, 81, 1, 23, "Print"],
Cell[11799, 401, 69, 1, 23, "Print"],
Cell[11871, 404, 83, 1, 23, "Print"],
Cell[11957, 407, 69, 1, 23, "Print"],
Cell[12029, 410, 81, 1, 23, "Print"],
Cell[12113, 413, 69, 1, 23, "Print"],
Cell[12185, 416, 81, 1, 23, "Print"],
Cell[12269, 419, 69, 1, 23, "Print"],
Cell[12341, 422, 81, 1, 23, "Print"],
Cell[12425, 425, 69, 1, 23, "Print"],
Cell[12497, 428, 80, 1, 23, "Print"],
Cell[12580, 431, 69, 1, 23, "Print"],
Cell[12652, 434, 81, 1, 23, "Print"],
Cell[12736, 437, 69, 1, 23, "Print"],
Cell[12808, 440, 81, 1, 23, "Print"],
Cell[12892, 443, 71, 1, 23, "Print"],
Cell[12966, 446, 83, 1, 23, "Print"],
Cell[13052, 449, 69, 1, 23, "Print"],
Cell[13124, 452, 81, 1, 23, "Print"],
Cell[13208, 455, 69, 1, 23, "Print"],
Cell[13280, 458, 83, 1, 23, "Print"],
Cell[13366, 461, 71, 1, 23, "Print"],
Cell[13440, 464, 81, 1, 23, "Print"],
Cell[13524, 467, 71, 1, 23, "Print"],
Cell[13598, 470, 81, 1, 23, "Print"],
Cell[13682, 473, 69, 1, 23, "Print"],
Cell[13754, 476, 81, 1, 23, "Print"],
Cell[13838, 479, 69, 1, 23, "Print"],
Cell[13910, 482, 81, 1, 23, "Print"],
Cell[13994, 485, 69, 1, 23, "Print"],
Cell[14066, 488, 80, 1, 23, "Print"],
Cell[14149, 491, 69, 1, 23, "Print"],
Cell[14221, 494, 79, 1, 23, "Print"],
Cell[14303, 497, 69, 1, 23, "Print"],
Cell[14375, 500, 81, 1, 23, "Print"],
Cell[14459, 503, 69, 1, 23, "Print"],
Cell[14531, 506, 83, 1, 23, "Print"],
Cell[14617, 509, 69, 1, 23, "Print"],
Cell[14689, 512, 81, 1, 23, "Print"],
Cell[14773, 515, 69, 1, 23, "Print"],
Cell[14845, 518, 82, 1, 23, "Print"],
Cell[14930, 521, 69, 1, 23, "Print"],
Cell[15002, 524, 82, 1, 23, "Print"],
Cell[15087, 527, 71, 1, 23, "Print"],
Cell[15161, 530, 84, 1, 23, "Print"],
Cell[15248, 533, 71, 1, 23, "Print"],
Cell[15322, 536, 84, 1, 23, "Print"],
Cell[15409, 539, 69, 1, 23, "Print"],
Cell[15481, 542, 82, 1, 23, "Print"],
Cell[15566, 545, 71, 1, 23, "Print"],
Cell[15640, 548, 81, 1, 23, "Print"],
Cell[15724, 551, 68, 1, 23, "Print"],
Cell[15795, 554, 82, 1, 23, "Print"],
Cell[15880, 557, 69, 1, 23, "Print"],
Cell[15952, 560, 80, 1, 23, "Print"],
Cell[16035, 563, 71, 1, 23, "Print"],
Cell[16109, 566, 82, 1, 23, "Print"],
Cell[16194, 569, 69, 1, 23, "Print"],
Cell[16266, 572, 82, 1, 23, "Print"],
Cell[16351, 575, 69, 1, 23, "Print"],
Cell[16423, 578, 84, 1, 23, "Print"],
Cell[16510, 581, 68, 1, 23, "Print"],
Cell[16581, 584, 82, 1, 23, "Print"],
Cell[16666, 587, 69, 1, 23, "Print"],
Cell[16738, 590, 81, 1, 23, "Print"],
Cell[16822, 593, 69, 1, 23, "Print"],
Cell[16894, 596, 81, 1, 23, "Print"],
Cell[16978, 599, 69, 1, 23, "Print"],
Cell[17050, 602, 82, 1, 23, "Print"],
Cell[17135, 605, 69, 1, 23, "Print"],
Cell[17207, 608, 81, 1, 23, "Print"],
Cell[17291, 611, 71, 1, 23, "Print"],
Cell[17365, 614, 82, 1, 23, "Print"],
Cell[17450, 617, 69, 1, 23, "Print"],
Cell[17522, 620, 82, 1, 23, "Print"],
Cell[17607, 623, 69, 1, 23, "Print"],
Cell[17679, 626, 82, 1, 23, "Print"],
Cell[17764, 629, 69, 1, 23, "Print"],
Cell[17836, 632, 82, 1, 23, "Print"],
Cell[17921, 635, 69, 1, 23, "Print"],
Cell[17993, 638, 82, 1, 23, "Print"],
Cell[18078, 641, 71, 1, 23, "Print"],
Cell[18152, 644, 82, 1, 23, "Print"],
Cell[18237, 647, 69, 1, 23, "Print"],
Cell[18309, 650, 82, 1, 23, "Print"],
Cell[18394, 653, 71, 1, 23, "Print"],
Cell[18468, 656, 82, 1, 23, "Print"],
Cell[18553, 659, 69, 1, 23, "Print"],
Cell[18625, 662, 81, 1, 23, "Print"],
Cell[18709, 665, 69, 1, 23, "Print"],
Cell[18781, 668, 82, 1, 23, "Print"],
Cell[18866, 671, 68, 1, 23, "Print"],
Cell[18937, 674, 82, 1, 23, "Print"],
Cell[19022, 677, 69, 1, 23, "Print"],
Cell[19094, 680, 84, 1, 23, "Print"],
Cell[19181, 683, 69, 1, 23, "Print"],
Cell[19253, 686, 82, 1, 23, "Print"],
Cell[19338, 689, 71, 1, 23, "Print"],
Cell[19412, 692, 82, 1, 23, "Print"],
Cell[19497, 695, 69, 1, 23, "Print"],
Cell[19569, 698, 80, 1, 23, "Print"],
Cell[19652, 701, 69, 1, 23, "Print"],
Cell[19724, 704, 84, 1, 23, "Print"],
Cell[19811, 707, 69, 1, 23, "Print"],
Cell[19883, 710, 82, 1, 23, "Print"],
Cell[19968, 713, 71, 1, 23, "Print"],
Cell[20042, 716, 82, 1, 23, "Print"],
Cell[20127, 719, 69, 1, 23, "Print"],
Cell[20199, 722, 82, 1, 23, "Print"]
}, Open  ]],
Cell[20296, 726, 183, 3, 50, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

